import scrapy
from walmart.items import WalmartItem
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from scrapy.http import HtmlResponse
from scrapy.http import TextResponse
from lxml import html
from lxml import etree
import csv
import unicodedata
from selenium.webdriver.common.by import By
import time

class DictUnicodeProxy(object):
    def __init__(self, d):
        self.d = d
    def __iter__(self):
        return self.d.__iter__()
    def get(self, item, default=None):
        i = self.d.get(item, default)
        if isinstance(i, unicode):
            return i.encode('utf-8')
        return i
class WalmartSpider(scrapy.Spider):
    name = "walmart"

    def __init__(self):
        chrome_driver = "walmart/chromedriver/chromedriver.exe"
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')  # Last I checked this was necessary
        self.driver = webdriver.Chrome(chrome_driver, chrome_options = options)
        self.start_urls = ['https://www.walmart.com/browse/3944/?cat_id=3944&facet=condition%3ANew%7C%7Cretailer%3AWalmart.com%7C%7Cspecial_offers%3AAs+Advertised%7C%7Cspecial_offers%3AClearance%7C%7Cspecial_offers%3ANew%7C%7Cspecial_offers%3AReduced+Price%7C%7Cspecial_offers%3ARollback%7C%7Cspecial_offers%3ASpecial+Buy%7C%7Ccondition%3ARefurbished&grid=true&max_price=150&min_price=50&page=1&sort=best_seller&stores=-1#searchProductResult']

    def parse(self, response):
        self.driver.get(response.url)
        print("-------------------------------start------------------------\n")
        items = self.parseItem()
        for item in items:
            print("\nTitle : "+item["Title"]+"\n")
        print(" -------------------------------end-------------------------\n")
        self.driver.close()
    
    def parseItem(self):
        items = []
        #each trip div has desribed two trip legs, we have to create 2 elements for each div
        with open('result/data.csv', 'wb') as csvfile:
            fieldnames = ["PartNum", "Url", "Title", "Manufacturer", "WalmartNum", "Description", "ListPrice", "SalePrice", "ImageURL", "ImageFile", "Quantity", "Depth", "Height", "Weight", "Width", "Catagory", "SpecialType", "shippingType"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames, lineterminator='\n')
            writer.writeheader()
            sIndex = 0
            while(1):
                li = self.driver.find_elements_by_xpath('//*[@id="searchProductResult"]/ul/li')[sIndex%40]
                item = WalmartItem()
                try:
                    specialType = li.find_elements_by_xpath(".//div[@class='prod-FlagList-container']/span[contains(@class, 'flag')]")[0].text
                    pass
                except Exception as e:
                    specialType = ""
                    pass
                try:
                    item['Url'] = (li.find_elements_by_xpath(".//a[@class='product-title-link line-clamp line-clamp-2']"))[0].get_attribute("href")
                    pass
                except Exception as e:
                    item['Url'] = ""
                    pass
                try:
                    item['Title'] = (li.find_elements_by_xpath(".//a[@class='product-title-link line-clamp line-clamp-2']/span"))[0].text
                    pass
                except Exception as e:
                    item['Title'] = ""
                    pass


                try:
                    imgUrl = (li.find_elements_by_xpath(".//img[contains(@class,'Tile-img')]"))[0].get_attribute("src")
                    pass
                except Exception as e:
                    imgUrl = ""
                    pass
                try:
                    salePrice = (li.find_elements_by_xpath(".//span[@class='price-characteristic']"))[0].text+"."+(li.find_elements_by_xpath(".//span[@class='price-mantissa']"))[0].text
                    pass
                except Exception as e:
                    salePrice = ""
                    pass
                try:
                    listPrice = (li.find_elements_by_xpath(".//span[@class='price-characteristic']"))[1].text+"."+(li.find_elements_by_xpath(".//span[@class='price-mantissa']"))[1].text
                    pass
                except Exception as e:
                    listPrice = ""
                    pass
                try:
                    shippingType = (li.find_elements_by_xpath(".//div[@data-tl-id='shipping-message-tile-shippingMessage']"))[0].text
                    pass
                except Exception as e:
                    shippingType = ""
                    pass
                try:
                    (li.find_elements_by_xpath(".//a[@class='product-title-link line-clamp line-clamp-2']"))[0].click()
                    pass
                except Exception as e:
                    pass

                try:
                    item['Manufacturer'] = self.driver.find_elements_by_xpath(".//a[@class='prod-brandName']/span[@itemprop='brand']")[0].text
                    pass
                except Exception as e:
                    item['Manufacturer'] = ""
                    pass

                try:
                    item['WalmartNum'] = self.driver.find_elements_by_xpath(".//div[contains(@class,'display-inline-block wm-item-number')]")[0].get_attribute("aria-label")
                    item['WalmartNum'] = item['WalmartNum'][15:]
                    pass
                except Exception as e:
                    item['WalmartNum'] = ""
                    pass
                try:
                    item['Description'] = self.driver.find_elements_by_xpath(".//div[@data-tl-id='AboutThis-ShortDescription']")[0].text
                    pass
                except Exception as e:
                    item['Description'] = ""
                    pass
                try:
                    item['ListPrice'] = listPrice
                    pass
                except Exception as e:
                    item['ListPrice'] = ""
                    pass
                try:
                    item['SalePrice'] = salePrice
                    pass
                except Exception as e:
                    item['SalePrice'] = ""
                    pass
                try:
                    item['ImageURL'] = self.driver.find_elements_by_xpath("//img[contains(@class,'prod-hero-image-image')]")[0].get_attribute("src")
                    pass
                except Exception as e:
                    item['ImageURL'] = ""
                    pass
                try:
                    imgfile = self.driver.find_elements_by_xpath("//img[contains(@class,'prod-hero-image-image')]")[0].get_attribute("src")
                    sPos = imgfile.find("?")
                    item['ImageFile'] = imgfile[33:sPos]
                    pass
                except Exception as e:
                    item['ImageFile'] = ""
                    pass
                try:
                    item['Quantity'] = self.driver.find_elements_by_xpath('//*[@id="quantity"]/option')[-1].text
                    pass
                except Exception as e:
                    item['Quantity'] = ""
                    pass

                try:
                    (self.driver.find_elements_by_xpath("//span[contains(text(), 'Specifications')]"))[0].click()
                    pass
                except Exception as e:
                    pass

                try:
                    spec_div = self.driver.find_elements_by_xpath('//div[@class="Specifications"]')[0]
                    pass
                except Exception as e:
                    pass
                #temp_div = spec_div.find_elements_by_xpath("//th[contains(text(), 'Display Technology')]")[0].find_elements_by_xpath("//following-sibling::td")[0].find_elements_by_xpath("//div")[0].text
                """depth = spec_div.find_elements_by_xpath("//th[contains(text(), 'Assembled Product Weight') and (@class='display-name')]/following-sibling::td/div")[0].text
                print("--------------------------------test----------------------------------\n")
                print("----------------"+depth.encode('utf-8'))
                print("---------------------------------end----------------------------------\n")"""
                try:
                    depth = spec_div.find_elements_by_xpath("//th[contains(text(), 'Depth (with stand)') and (@class='display-name')]/following-sibling::td/div")[0].text
                    item['Depth'] = depth.encode('utf-8')
                    pass
                except Exception as e:
                    pass

                try:
                    depth = spec_div.find_elements_by_xpath("//th[contains(text(), 'Manufacturer Part Number') and (@class='display-name')]/following-sibling::td/div")[0].text
                    item['PartNum'] = depth.encode('utf-8')
                    pass
                except Exception as e:
                    pass

                try:
                    height = spec_div.find_elements_by_xpath("//th[contains(text(), 'Height (with stand)') and (@class='display-name')]/following-sibling::td/div")[0].text
                    item['Height'] = height.encode('utf-8')
                    pass
                except Exception as e:
                    pass

                try:
                    weight = spec_div.find_elements_by_xpath("//th[contains(text(), 'Assembled Product Weight') and (@class='display-name')]/following-sibling::td/div")[0].text
                    item['Weight'] = weight.encode('utf-8')
                    pass
                except Exception as e:
                    pass

                try:
                    width = spec_div.find_elements_by_xpath("//th[contains(text(), 'Width (with stand)') and (@class='display-name')]/following-sibling::td/div")[0].text
                    item['Width'] = width.encode('utf-8')
                    pass
                except Exception as e:
                    pass

                try:
                    category_ol = self.driver.find_elements_by_xpath('//ol[@class="breadcrumb-list "]/li/a')
                    catagory = ""
                    for ol_li in category_ol:
                        catagory = catagory + ol_li.text+" / "#(ol_li.find_elements_by_xpath("//a[@itemprop='item']"))[0].text+" / "
                    catagory = catagory[:-3]
                    pass
                except Exception as e:
                    catagory = ""
                    pass

                item['Catagory'] = catagory
                item['SpecialType'] = specialType
                if shippingType== "2-day shipping":
                    shippingType = "Free Shipping"
                item['shippingType'] = shippingType

                self.driver.back()
                writer.writerow(DictUnicodeProxy(item))

                items.append(item)
                sIndex+=1
                if sIndex/40 == 25:
                    break
                if sIndex%40==0:
                    sPage = sIndex/40+1
                    self.driver.get("https://www.walmart.com/browse/3944/?cat_id=3944&facet=condition%3ANew%7C%7Cretailer%3AWalmart.com%7C%7Cspecial_offers%3AAs+Advertised%7C%7Cspecial_offers%3AClearance%7C%7Cspecial_offers%3ANew%7C%7Cspecial_offers%3AReduced+Price%7C%7Cspecial_offers%3ARollback%7C%7Cspecial_offers%3ASpecial+Buy%7C%7Ccondition%3ARefurbished&grid=true&max_price=150&min_price=50&page="+str(sPage)+"&sort=best_seller&stores=-1#searchProductResult")
                    
            csvfile.close()        
        return items
