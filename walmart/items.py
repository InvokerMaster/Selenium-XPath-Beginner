# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class WalmartItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    PartNum = scrapy.Field()
    Url = scrapy.Field()
    Title = scrapy.Field()
    Manufacturer = scrapy.Field()
    WalmartNum = scrapy.Field()
    Description = scrapy.Field()
    ListPrice = scrapy.Field()
    SalePrice = scrapy.Field()
    ImageURL = scrapy.Field()
    ImageFile = scrapy.Field()
    Quantity = scrapy.Field()
    Depth = scrapy.Field()
    Height = scrapy.Field()
    Weight = scrapy.Field()
    Width = scrapy.Field()
    Catagory = scrapy.Field()
    SpecialType = scrapy.Field()
    shippingType = scrapy.Field()
    pass