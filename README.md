# Simple Selenium Project using XPath

This is Easy Understanding Selenium Project for beginners who want to scrape values and export to CSV.
Mainly for giving idea of using xpath to get elements

### Find By Class Name

Multiple classes are allowed.
Second is the selector to select element that includes class. <br> `contains` can be used not only for class but other attributes

```
find_elements_by_xpath(".//a[@class='class1 class2']")
find_elements_by_xpath(".//a[contains(@class,'classname')]"))
```

### Get Element inside specific class

```
find_elements_by_xpath(".//a[@class='myclass']/span")
```

### Get Image Src attribute

```
find_elements_by_xpath(".//img[contains(@class,'Tile-img')]"))[0].get_attribute("src")
```

### Get Inner HTML

```
find_elements_by_xpath(".//div[@someattr='someattrvalue']"))[0].text
```

### Get element which has id of "somevalue"

```
find_elements_by_xpath('//*[@id="somevalue"]')
```

### Get element which has content that includes "somevalue"

```
find_elements_by_xpath("//span[contains(text(), 'somevalue')]")
```

## Authors

* **Invoker** - *Profile* - [InvokerMaster](https://github.com/InvokerMaster)

